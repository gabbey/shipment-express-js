var express = require("express");
var bodyParser = require('body-parser');
var server = express();
var apiRouter = require('./apiRouter').router;
require('dotenv').config();


server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

server.get('/', function (req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send('<h1>Shipment server in online</h1>')
});

server.use('/api/', apiRouter);

var server_port = process.env.PORT;
server.listen(server_port, () => {
    console.log(`Server start and listening on port ${server_port}`);
})