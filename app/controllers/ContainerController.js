var models = require('../../models');
const jwt = require('../../utilities/jwt');
const Joi = require('joi');

module.exports = {
    index: function (request, response) {
        let bearer_token = request.headers['authorization'];
        if (!bearer_token) {
            return response.status(401).json({ status: false, error: 'Please provide a token' });
        }
        let auth_user_id = jwt.getAuthUserId(bearer_token);
        if (!auth_user_id) {
            return response.status(401).json({ status: false, error: 'Invalid token' });
        }

        const validation_schema = Joi.object({
            shipment_id: Joi.number().required(),
            page: Joi.number().required(),
            limit: Joi.number().required(),
        });

        const options = {
            abortEarly: false,
            allowUnknown: false,
            stripUnknown: true
        };

        const validation = { error, value } = validation_schema.validate(request.query, options);

        if (validation.error) {
            let errors = validation.error.details.map(x => {
                return { field: x.context.key, type: x.type, error: x.message };
            });
            return response.status(422).json({ "errors": errors })
        } else {
            const shipment_id = request.query.shipment_id ? parseInt(request.query.shipment_id) : null;
            const page = request.query.page ? parseInt(request.query.page) : 1;
            const limit = request.query.limit ? parseInt(request.query.limit) : 10;
            const offset = (page - 1) * limit;

            models.Container.findAll({
                order: [['id', 'ASC']],
                attributes: ['id', 'name'],
                limit: limit,
                offset: offset,
                where: { shipmentId: shipment_id }
            }).then(function (containers) {
                return response.status(200).json({ page, limit, containers });
            }).catch(function (error) {
                return response.status(500).json({ status: false, 'error': error });
            })
        }
    }
}