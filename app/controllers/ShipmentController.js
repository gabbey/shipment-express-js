var models = require('../../models');
var asyncLib = require('async');
const Joi = require('joi');
const jwt = require('../../utilities/jwt');

module.exports = {
    store: async function (request, response) {
        let bearer_token = request.headers['authorization'];
        if (!bearer_token) {
            return response.status(401).json({ status: false, error: 'Please provide a token' });
        }
        let auth_user_id = jwt.getAuthUserId(bearer_token);
        if (!auth_user_id) {
            return response.status(401).json({ status: false, error: 'Invalid token' });
        }
        let user = await models.User.findOne({
            attributes: ['id', 'name', 'email'],
            where: { id: auth_user_id }
        })
        const validation_schema = Joi.object({
            origin: Joi.string().required(),
            destination: Joi.required(),
            description: Joi.string(),
            containers: Joi.array().items({
                name: Joi.string().required(),
                tracking_steps: Joi.array().items({
                    name: Joi.string().required()
                }).required()
            }).required()
        });

        const options = {
            abortEarly: false,
            allowUnknown: false,
            stripUnknown: true
        };

        const validation = { error, value } = validation_schema.validate(request.body, options);

        if (validation.error) {
            let errors = validation.error.details.map(x => {
                return { field: x.context.key, type: x.type, error: x.message };
            });
            return response.status(422).json({ "errors": errors })
        } else {
            let origin = request.body.origin;
            let destination = request.body.destination;
            let description = request.body.description;
            let containers = request.body.containers;

            asyncLib.waterfall([
                function (resolve) {
                    models.Shipment.create({
                        userId: auth_user_id,
                        origin: origin,
                        destination: destination,
                        description: description
                    }).then((shipment) => {
                        resolve(null, shipment, containers);
                    }).catch((error) => {
                        return response.status(500).json({ status: false, error: error });
                    })
                },
                function (shipment, containers, resolve) {
                    if (shipment) {
                        try {
                            containers.forEach(element => {
                                models.Container.create({
                                    shipmentId: shipment.id,
                                    name: element.name
                                }).then((container) => {
                                    let steps = element.tracking_steps;
                                    steps.forEach(step => {
                                        models.TrackingStep.create({
                                            containerId: container.id,
                                            name: step.name,
                                            status: '0'
                                        });
                                    });
                                });
                            });
                            resolve(shipment);
                        } catch (error) {
                            return response.status(500).json({ status: false, error: error });
                        }
                    }
                }
            ], function (shipment) {

                return response.status(200).json({
                    id: shipment.id,
                    user: user,
                    origin: shipment.origin,
                    destination: shipment.destination,
                    description: description,
                    total_containers: containers.length
                });
            });


        }
    },
    index: function (request, response) {
        let bearer_token = request.headers['authorization'];
        if (!bearer_token) {
            return response.status(401).json({ status: false, error: 'Please provide a token' });
        }
        let auth_user_id = jwt.getAuthUserId(bearer_token);
        if (!auth_user_id) {
            return response.status(401).json({ status: false, error: 'Invalid token' });
        }

        const validation_schema = Joi.object({
            page: Joi.number().required(),
            limit: Joi.number().required(),
        });

        const options = {
            abortEarly: false,
            allowUnknown: false,
            stripUnknown: true
        };

        const validation = { error, value } = validation_schema.validate(request.query, options);

        if (validation.error) {
            let errors = validation.error.details.map(x => {
                return { field: x.context.key, type: x.type, error: x.message };
            });
            return response.status(422).json({ "errors": errors })
        } else {
            const page = request.query.page ? parseInt(request.query.page) : 1;
            const limit = request.query.limit ? parseInt(request.query.limit) : 10;
            const offset = (page - 1) * limit;

            models.Shipment.findAll({
                order: [['id', 'ASC']],
                attributes: ['id', 'origin', 'destination', 'description'],
                limit: limit,
                offset: offset,
                include: [{ model: models.Container, attributes: ['id', 'name', 'createdAt', 'updatedAt'] }]
            }).then(function (shipments) {
                return response.status(200).json({ page, limit, shipments });
            }).catch(function (error) {
                return response.status(500).json({ status: false, 'error': error });
            })
        }

    }
}