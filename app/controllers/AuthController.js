var bcrypt = require('bcrypt');
var jwtUtilities = require('../../utilities/jwt');
var models = require('../../models');
var asyncLib = require('async');
const Joi = require('joi');

module.exports = {
    login: function (request, response) {
        const validation_schema = Joi.object({
            email: Joi.string().required().email(),
            password: Joi.required()
        });

        const options = {
            abortEarly: false,
            allowUnknown: false,
            stripUnknown: true
        };

        const validation = { error, value } = validation_schema.validate(request.body, options);

        if (validation.error) {
            let errors = validation.error.details.map(x => {
                return { field: x.context.key, error: x.message };
            });
            return response.status(422).json({ "errors": errors })
        } else {
            asyncLib.waterfall([
                function (resolve) {
                    models.User.findOne({
                        attributes: ['id', 'email', 'password'],
                        where: { email: request.body.email }
                    }).then((user) => {
                        resolve(null, user);
                    }).catch((error) => {
                        return response.status(500).json({ status: false, error: error });
                    })
                },
                function (user, resolve) {
                    if (user) {
                        bcrypt.compare(request.body.password, user.password, function (errBcrypt, responseBcrypt) {
                            resolve(null, user, responseBcrypt);
                        });
                    } else {
                        return response.status(422).json({ status: false, error: 'Invalid email or password' });
                    }
                },
                function (user, responseBcrypt, resolve) {
                    if (responseBcrypt) {
                        let token = jwtUtilities.generateTokenForUser(user);
                        resolve(user, token);
                    } else {
                        return response.status(422).json({ status: false, error: 'Invalid email or passwordn' });
                    }
                }
            ], function (user, token) {
                return response.status(200).json({
                    user: {
                        id: user.id,
                        email: user.email,
                        username: user.username,
                        bio: user.bio
                    },
                    access_token: token
                });
            });


        }
    }
}