var models = require('../../models');
const jwt = require('../../utilities/jwt');
const Joi = require('joi');
const asyncLib = require('async');

module.exports = {
    update: function (request, response) {
        let bearer_token = request.headers['authorization'];
        if (!bearer_token) {
            return response.status(401).json({ status: false, error: 'Please provide a token' });
        }
        let auth_user_id = jwt.getAuthUserId(bearer_token);
        if (!auth_user_id) {
            return response.status(401).json({ status: false, error: 'Invalid token' });
        }

        const validation_schema = Joi.object({
            id: Joi.required(),
            name: Joi.string(),
            status: Joi.valid('1', '2').required()
        });

        const options = {
            abortEarly: false,
            allowUnknown: false,
            stripUnknown: true
        };

        const validation = { error, value } = validation_schema.validate(request.body, options);

        if (validation.error) {
            let errors = validation.error.details.map(x => {
                return { field: x.context.key, type: x.type, error: x.message };
            });
            return response.status(422).json({ "errors": errors })
        } else {
            let id = request.body.id;
            let name = request.body.name;
            let status = request.body.status;

            try {
                asyncLib.waterfall([
                    function (resolve) {
                        models.TrackingStep.findOne({
                            attributes: ['id', 'name', 'containerId', 'status'],
                            where: { id: id }
                        }).then((tracking_step) => {
                            resolve(null, tracking_step);
                        })
                    },
                    function (tracking_step, resolve) {
                        if (tracking_step) {
                            if (status == '2') {
                                console.log('tracking_step.containerId');
                                models.TrackingStep.findAll({
                                    order: [['id', 'ASC']],
                                    attributes: ['id', 'name', 'status'],
                                    where: { containerId: tracking_step.containerId }
                                }).then((results) => {
                                    if (results.length > 0) {
                                        let step_index = null;
                                        results.forEach((element, index) => {
                                            if (element.id == id) step_index = index;
                                        });

                                        if (step_index > 0) {
                                            let previous_step = results[step_index - 1];
                                            // console.log( previous_step.status);
                                            if (previous_step.status == '2') {
                                                resolve(null, tracking_step, results, step_index + 1);
                                            } else return response.status(200).json({ status: false, error: 'You have to complete the previous tracking step before completing this step' })
                                        } else resolve(null, tracking_step, results, step_index + 1);
                                    }
                                });

                            } else resolve(null, tracking_step, null, null);
                        } else return response.status(404).json({ status: false, error: 'Invalid step id provided' });
                    },
                    function (tracking_step, steps, next_step_index, resolve) {
                        //TODO: define the completed_at date when status is set to 2
                        tracking_step.update({
                            name: name ? name : tracking_step.name,
                            status: status ? status : tracking_step.status
                        }).then((updated_step) => {
                            if (steps && next_step_index) {
                                let next_step = steps[next_step_index];
                                if (next_step !== undefined) {
                                    models.TrackingStep.findOne({
                                        attributes: ['id'],
                                        where: { id: next_step.id }
                                    }).then((found_step) => {
                                        found_step.update({
                                            status: '1'
                                        });
                                    })
                                }
                            }
                            resolve(updated_step);
                        });
                    }
                ], function (updated_step) {
                    return response.status(200).json({
                        id: updated_step.id,
                        name: updated_step.name,
                        status: module.exports.getLibelleStatus(updated_step.status)
                    });
                });
            } catch (error) {
                return response.status(500).json({ "errors": error })
            }
        }
    },
    getLibelleStatus: function (status) {
        let status_libelle = '';
        switch (status) {
            case '0':
                status_libelle = 'Not started';
                break;
            case '1':
                status_libelle = 'In Progress';
                break;
            case '2':
                status_libelle = 'Complete';
                break;
            default:
                break;
        }
        return status_libelle;
    }
}