var express = require('express');
var AuthController = require('./app/controllers/AuthController');
var ShipmentController = require('./app/controllers/ShipmentController');
var ContainerController = require('./app/controllers/ContainerController');
var TrackingStepController = require('./app/controllers/TrackingStepController');

exports.router = (function () {
    let apiRouter = express.Router();

    //TODO define middleware to check if user is authenticated instead of doing it in the controllers
    //auth routes
    apiRouter.route('/auth/login/').post(AuthController.login);

    //shipments routes
    apiRouter.route('/shipments/').post(ShipmentController.store);
    apiRouter.route('/shipments/').get(ShipmentController.index);

    //tracking steps routes
    apiRouter.route('/tracking-steps/').put(TrackingStepController.update);

    //containers routes
    apiRouter.route('/containers/').get(ContainerController.index);

    return apiRouter;
})();