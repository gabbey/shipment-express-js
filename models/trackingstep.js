'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TrackingStep extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.TrackingStep.belongsTo(models.Container, {
        foreignKey: {
          name: 'containerId',
          allowNull: false,
        }
      })
    }
  }
  TrackingStep.init({
    containerId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    status: DataTypes.ENUM('0', '1', '2'),
    completedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'TrackingStep',
  });
  return TrackingStep;
};