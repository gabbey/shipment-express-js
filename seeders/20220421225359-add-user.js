'use strict';
var bcrypt = require('bcrypt');

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Users', [
      {
        name: 'John Doe',
        email: 'john@mail.com',
        password: await bcrypt.hash('secret', 5),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Samantha Doe',
        email: 'samantha@mail.com',
        password: await bcrypt.hash('secret', 5),
        createdAt: new Date(),
        updatedAt: new Date(),
      }], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
