var jwt = require('jsonwebtoken');
require('dotenv').config();

const JWT_SECRET = process.env.JWT_SECRET;
module.exports = {
    generateTokenForUser: function (userData) {
        return jwt.sign({
            user_id: userData.id
        }, JWT_SECRET, {
            expiresIn: '1h'
        });
    },
    parseAuthorisation: function(authorisation) {
        return (authorisation != null) ? authorisation.replace('Bearer ', '') : null;
    },
    getAuthUserId: function(authorisation) {
        let user_id = null;
        let token = module.exports.parseAuthorisation(authorisation);
        if (token) {
            try {
                let jwt_token = jwt.verify(token, JWT_SECRET);
                if (jwt_token) {
                    user_id = jwt_token.user_id;
                }
            } catch (error) {}
        }
        return user_id;
    }
}